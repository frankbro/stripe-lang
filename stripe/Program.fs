﻿module Stripe.Program

open System
open System.Collections.Generic

open Expr
open Infer

(*
let replace_ty_constants_with_vars var_name_list ty =
    let name_map = ref Map.empty 
    let var_id_list_rev_ref = ref []
    var_name_list
    |> List.iter (fun var_name -> name_map := (!name_map).Add(var_name, None))
    let rec f = function
        | TConst name as ty ->
            try
                match (!name_map).Item name with
                | Some var -> var
                | None ->
                    let var_id, var = new_bound_var ()
                    var_id_list_rev_ref := var_id :: !var_id_list_rev_ref
                    name_map := (!name_map).Add(name, Some var)
                    var
            with
            | :? KeyNotFoundException as ex -> ty
        | TVar _ as ty -> ty
        | TApp(ty, ty_arg_list) ->
            let new_ty = f ty
            let new_ty_arg_list = 
                ty_arg_list
                |> List.map f
            TApp(new_ty, new_ty_arg_list)
        | TArrow(param_ty_list, return_ty) ->
            let new_param_ty_list =
                param_ty_list
                |> List.map f
            let new_return_ty = f return_ty
            TArrow(new_param_ty_list, new_return_ty)
        | TForall(var_id_list, ty) -> TForall(var_id_list, f ty)
    (List.rev !var_id_list_rev_ref, f ty)
*)

[<EntryPoint>]
let main argv = 
    printfn "%A" argv
    0 // return an integer exit code
