﻿module Stripe.Propagate

open System
open System.Collections.Generic

open Expr
open Infer
open Util

type State = 
    | Generalized 
    | Instantiated

let rec should_generalize = function
    | TForall _ -> Generalized
    | TVar {contents = Link ty} -> should_generalize ty
    | _ -> Instantiated

let maybe_generalize level ty = function
    | Instantiated -> ty
    | Generalized -> generalize level ty

let maybe_instantiate level ty = function
    | Instantiated -> instantiate level ty
    | Generalized -> ty

let generalize_or_instantiate level ty = function
    | Instantiated -> instantiate level ty
    | Generalized -> generalize level ty

let rec infer (env: Map<_,_>) level maybe_expected_ty generalized = function
    | Var name -> 
        try
            maybe_instantiate level (env.Item(name)) generalized
        with
        | :? KeyNotFoundException -> error("variable " + name + " not found")
    | Fun(param_list, body_expr) ->
        let expected_param_list, maybe_expected_return_ty, body_generalized =
            match maybe_expected_ty with
            | None -> param_list, None, Instantiated
            | Some expected_ty ->
                match instantiate (level + 1) expected_ty with
                | TArrow(expected_param_ty_list, expected_return_ty) ->
                    TODO "This is ugly, reimplement"
                    List.map2 
                        (fun (param_name, maybe_param_ty_ann) expected_param_ty ->
                            param_name, 
                                match maybe_param_ty_ann with 
                                | None -> Some ([], expected_param_ty)
                                | _ -> maybe_param_ty_ann
                        )
                        param_list expected_param_ty_list, Some expected_return_ty, should_generalize expected_return_ty
                | _ -> param_list, None, Instantiated

        let fn_env_ref = ref env 
        let var_list_ref = ref []
        let param_ty_list = 
            expected_param_list
            |> List.map (fun (param_name, maybe_param_ty_ann) ->
                let param_ty =
                    match maybe_param_ty_ann with
                    | None -> // equivalent to `some[a] a`
                        let var = new_var (level + 1) 
                        var_list_ref := var :: !var_list_ref
                        var
                    | Some ty_ann ->
                        let var_list, ty = instantiate_ty_ann (level + 1) ty_ann
                        var_list_ref := var_list @ !var_list_ref
                        ty
                fn_env_ref := (!fn_env_ref).Add(param_name, param_ty)
                param_ty
            )
            
        let return_ty =
            infer !fn_env_ref (level + 1) maybe_expected_return_ty body_generalized body_expr

        if not (!var_list_ref |> List.forall is_monomorphic) then
            error <| "polymorphic parameter inferred: " + String.Concat(", ", (!var_list_ref |> List.map string_of_ty))
        else
            maybe_generalize level (TArrow(param_ty_list, return_ty)) generalized

    | Let(var_name, value_expr, body_expr) ->
        let var_ty = infer env (level + 1) None Generalized value_expr 
        infer (env.Add(var_name, var_ty)) level maybe_expected_ty generalized body_expr
    
    | Call(fn_expr, arg_list) ->
        let fn_ty = instantiate (level + 1) (infer env (level + 1) None Instantiated fn_expr)
        let param_ty_list, return_ty = match_fun_ty (List.length arg_list) fn_ty
        let instantiated_return_ty = instantiate (level + 1) return_ty
        match maybe_expected_ty, instantiated_return_ty with
        | None, _ | _, TVar {contents = Unbound _} -> ()
        | Some expected_ty, _ ->
            unify (instantiate (level + 1) expected_ty) instantiated_return_ty
        infer_args env (level + 1) param_ty_list arg_list
        generalize_or_instantiate level instantiated_return_ty generalized

    | Ann(expr, ty_ann) ->
        let _, ty = instantiate_ty_ann level ty_ann
        let expr_ty = infer env level (Some ty) (should_generalize ty) expr
        subsume level ty expr_ty
        ty

and infer_args env level param_ty_list arg_list =
    let pair_list = List.zip param_ty_list arg_list
    let get_ordering ty arg =
        // subsume annotated arguments first, type variables last
        if is_annotated arg then 
            0
        else
            match unlink ty with
            | TVar {contents = Unbound _} -> 2
            | _ -> 1
    let sorted_pair_list = 
        pair_list
        |> List.sortWith(fun (ty1, arg1) (ty2, arg2) -> 
            compare (get_ordering ty1 arg1) (get_ordering ty2 arg2)
        )

    sorted_pair_list
    |> List.iter (fun (param_ty, arg_expr) ->
        let arg_ty = infer env level (Some param_ty) (should_generalize param_ty) arg_expr
        if is_annotated arg_expr then
            unify param_ty arg_ty
        else
            subsume level param_ty arg_ty
    )