﻿module Stripe.Infer

open System
open System.Collections.Generic

open Expr
open Util

let int_map_from_2_lists key_list value_list =
    List.fold2
        (fun (map: Map<_,_>) key value -> map.Add(key, value))
        Map.empty key_list value_list

let int_map_remove_all map key_list =
    List.fold
        (fun (map: Map<_,_>) key -> map.Remove(key))
        map key_list

let current_id = ref 0

let next_id () =
    let id = !current_id 
    current_id := id + 1
    id

let reset_id () = current_id := 0

let new_var level = TVar (ref (Unbound(next_id (), level)))
let new_gen_var () = TVar (ref (Generic(next_id ())))
let new_bound_var () = 
    let id = next_id ()
    id, TVar (ref (Bound id))

exception Error of string
let error msg = raise (Error msg)

let occurs_check_adjust_levels tvar_id tvar_level ty =
    let rec f = function
        | TVar {contents = Link ty} -> f ty
        | TVar {contents = Generic _} | TVar {contents = Bound _} -> ()
        | TVar ({contents = Unbound(other_id, other_level)} as other_tvar) ->
            if other_id = tvar_id then
                TODO "We need to support recursive types"
                error "recursive types"
            else
                if other_level > tvar_level then
                    other_tvar := Unbound(other_id, tvar_level)
                else
                    ()
        | TApp(ty, ty_arg_list) ->
            f ty
            ty_arg_list |> List.iter f
        | TArrow(param_ty_list, return_ty) ->
            param_ty_list |> List.iter f
            f return_ty
        | TForall(_, ty) -> f ty
        | TConst _ -> ()
    f ty

let rec substitute_bound_vars var_id_list (ty_list: Type list) ty =
    let rec f (id_ty_map: Map<Id, Type>) = function
        | TConst _ as ty -> ty
        | TVar {contents = Link ty} -> f id_ty_map ty
        | TVar {contents = Bound id} as ty ->
            try
                id_ty_map.Item id
            with
            | :? KeyNotFoundException as ex -> ty
        | TVar _ as ty -> ty
        | TApp(ty, ty_arg_list) ->
            TApp(f id_ty_map ty, ty_arg_list |> List.map (f id_ty_map))
        | TArrow(param_ty_list, return_ty) ->
            TArrow(param_ty_list |> List.map (f id_ty_map), f id_ty_map return_ty)
        | TForall(var_id_list, ty) ->
            TForall(var_id_list, f (int_map_remove_all id_ty_map var_id_list) ty)
    f (int_map_from_2_lists var_id_list ty_list) ty

let free_generic_vars ty =
    TODO "Redo this so it's not mutable"
    let free_var_set = ref Set.empty
    let rec f = function
        | TConst _ -> ()
        | TVar {contents = Link ty} -> f ty
        | TVar {contents = Bound _ } -> ()
        | TVar {contents = Generic _ } as ty ->
            free_var_set := Set.add ty !free_var_set
        | TVar {contents = Unbound _ } -> ()
        | TApp(ty, ty_arg_list) ->
            f ty
            ty_arg_list |> List.iter f
        | TArrow(param_ty_list, return_ty) ->
            param_ty_list |> List.iter f
            f return_ty
        | TForall(_, ty) -> f ty
    f ty
    !free_var_set

let escape_check generic_var_list ty1 ty2 =
    let free_var_set1 = free_generic_vars ty1
    let free_var_set2 = free_generic_vars ty2
    generic_var_list
    |> List.exists
        (fun generic_var ->
            Set.contains generic_var free_var_set1 || Set.contains generic_var free_var_set2)

let rec unify ty1 ty2 =
    TODO "This was == in ocaml, which means physical equality, as opposed to structural equality"
    //if ty1 = ty2 then () else
    if Object.ReferenceEquals(ty1, ty2) then () else
    match ty1, ty2 with
    | TConst name1, TConst name2 when name1 = name2 -> ()
    | TApp(ty1, ty_arg_list1), TApp(ty2, ty_arg_list2) ->
        unify ty1 ty2
        List.iter2 unify ty_arg_list1 ty_arg_list2
    | TArrow(param_ty_list1, return_ty1), TArrow(param_ty_list2, return_ty2) ->
        List.iter2 unify param_ty_list1 param_ty_list2
        unify return_ty1 return_ty2
    | TVar {contents = Link ty1}, ty2 | ty1, TVar {contents = Link ty2} ->
        unify ty1 ty2
    | TVar {contents = Unbound(id1, _)}, TVar {contents = Unbound(id2, _)} 
    | TVar {contents = Generic id1}, TVar {contents = Generic id2} when id1 = id2 ->
        // This should be handled by the ty1 == ty2 case
        assert(false)
    | TVar {contents = Bound _}, _ | _, TVar {contents = Bound _} ->
        // Bound vars should have been instantiated
        assert(false)
    | TVar ({contents = Unbound(id, level)} as tvar), ty
    | ty, TVar ({contents = Unbound(id, level)} as tvar) ->
        occurs_check_adjust_levels id level ty
        tvar := Link ty
    | (TForall(var_id_list1, ty1) as forall_ty1), (TForall(var_id_list2, ty2) as forall_ty2) ->
        let generic_var_list =
            try
                List.map2 (fun _ _ -> new_gen_var ()) var_id_list1 var_id_list2
                |> List.rev
            with
            | ex -> 
                TODO "This was Invalid_argument in ocaml, would need to check what it is in F#"
                error <| "cannot unify types " + string_of_ty ty1 + " and " + string_of_ty ty2
        let generic_ty1 = substitute_bound_vars var_id_list1 generic_var_list ty1
        let generic_ty2 = substitute_bound_vars var_id_list2 generic_var_list ty2
        unify generic_ty1 generic_ty2
        if escape_check generic_var_list forall_ty1 forall_ty2 then
            error <| "cannot unify types " + string_of_ty forall_ty1 + " and " + string_of_ty forall_ty2
    | _, _ -> 
        error <| "cannot unify types " + string_of_ty ty1 + " and " + string_of_ty ty2

let substitute_with_new_vars level var_id_list ty =
    let var_list = 
        var_id_list
        |> List.map (fun _ -> new_var level)
        |> List.rev
    var_list, substitute_bound_vars var_id_list var_list ty

let instantiate_ty_ann level = function
    | [], ty -> [], ty
    | var_id_list, ty -> substitute_with_new_vars level var_id_list ty

let rec instantiate level = function
    | TForall(var_id_list, ty) ->
        let var_list, instantiated_ty = substitute_with_new_vars level var_id_list ty
        instantiated_ty
    | TVar {contents = Link ty} -> instantiate level ty
    | ty -> ty

let subsume level ty1 ty2 =
    let instantiated_ty2 = instantiate level ty2
    match unlink ty1 with
    | TForall(var_id_list1, ty1) as forall_ty1 ->
        let generic_var_list =
            var_id_list1
            |> List.map (fun _ -> new_gen_var ())
            |> List.rev
        let generic_ty1 = substitute_bound_vars var_id_list1 generic_var_list ty1
        unify generic_ty1 instantiated_ty2
        if escape_check generic_var_list forall_ty1 ty2 then
            error <| "type " + string_of_ty ty2 + " is not an instance of " + string_of_ty forall_ty1
    | ty1 -> unify ty1 instantiated_ty2

let generalize level ty =
    let var_id_list_rev_ref = ref []
    let rec f = function
        | TVar {contents = Link ty} -> f ty
        | TVar {contents = Generic _} -> assert false
        | TVar {contents = Bound _} -> ()
        | TVar ({contents = Unbound(other_id, other_level)} as other_tvar) when other_level > level ->
            other_tvar := Bound(other_id)
            if not <| List.exists((=) other_id) !var_id_list_rev_ref then
                var_id_list_rev_ref := other_id :: !var_id_list_rev_ref
        | TVar {contents = Unbound _} -> ()
        | TApp(ty, ty_arg_list) ->
            f ty
            ty_arg_list |> List.iter f
        | TArrow(param_ty_list, return_ty) ->
            param_ty_list |> List.iter f
            f return_ty
        | TForall(_, ty) -> f ty
        | TConst _ -> ()
    f ty
    match !var_id_list_rev_ref with
    | [] -> ty
    | var_id_list_rev -> TForall(List.rev var_id_list_rev, ty)

let rec match_fun_ty num_params = function
    | TArrow(param_ty_list, return_ty) ->
        if List.length param_ty_list <> num_params then
            error "unexpected number of arguments"
        else
            param_ty_list, return_ty
    | TVar {contents = Link ty} -> match_fun_ty num_params ty
    | TVar ({contents = Unbound(id, level)} as tvar) ->
        let param_ty_list =
            let rec f acc = function
                | 0 -> acc
                | n -> f (new_var level :: acc) (n - 1)
            f [] num_params
        let return_ty = new_var level
        tvar := Link (TArrow(param_ty_list, return_ty))
        param_ty_list, return_ty
    | _ -> error "expected a function"

let rec infer (env: Map<string, Type>) level = function
    | Var name ->
        try
            env.Item name
        with
        | :? KeyNotFoundException as ex -> error <| "variable " + name + " not found"
    | Fun(param_list, body_expr) ->
        let fn_env_ref = ref env 
        let var_list_ref = ref []
        let param_ty_list = 
            param_list
            |> List.map (fun (param_name, maybe_param_ty_ann) ->
                let param_ty = 
                    match maybe_param_ty_ann with
                    | None -> // equivalent of `some[a] a`
                        let var = new_var (level + 1)
                        var_list_ref := var :: !var_list_ref
                        var
                    | Some ty_ann ->
                        let var_list, ty = instantiate_ty_ann (level + 1) ty_ann
                        var_list_ref := var_list @ !var_list_ref
                        ty
                fn_env_ref := (!fn_env_ref).Add(param_name, param_ty)
                param_ty
            )
        let inferred_return_ty = infer !fn_env_ref (level + 1) body_expr
        let return_ty =
            if is_annotated body_expr then
                inferred_return_ty
            else
                instantiate (level + 1) inferred_return_ty
        if not (!var_list_ref |> List.forall is_monomorphic) then
            error <| "polymorphic parameter inferred: " + String.concat ", " (!var_list_ref |> List.map string_of_ty)
        else
            generalize level (TArrow(param_ty_list, return_ty))
    | Let(var_name, value_expr, body_expr) ->
        let var_ty = infer env (level + 1) value_expr
        infer (env.Add(var_name, var_ty)) level body_expr
    | Call(fn_expr, arg_list) ->
        let fn_ty = instantiate (level + 1) (infer env (level + 1) fn_expr)
        let param_ty_list, return_ty = match_fun_ty (List.length arg_list) fn_ty
        infer_args env (level + 1) param_ty_list arg_list
        generalize level (instantiate (level + 1) return_ty)
    | Ann(expr, ty_ann) ->
        let _, ty = instantiate_ty_ann level ty_ann
        let expr_ty = infer env level expr
        subsume level ty expr_ty
        ty

and infer_args env level param_ty_list arg_list =
    let pair_list = List.zip param_ty_list arg_list
    let get_ordering ty arg =
        // subsume type variables last
        match unlink ty with
        | TVar {contents = Unbound _} -> 1
        | _ -> 0
    let sorted_pair_list = 
        pair_list
        |> List.sortWith (fun (ty1, arg1) (ty2, arg2) -> 
            compare (get_ordering ty1 arg1) (get_ordering ty2 arg2)
        )
    sorted_pair_list
    |> List.iter (fun (param_ty, arg_expr) ->
        let arg_ty = infer env level arg_expr
        if is_annotated arg_expr then
            unify param_ty arg_ty
        else
            subsume level param_ty arg_ty
    )
