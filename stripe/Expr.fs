﻿module Stripe.Expr

open System

open Util

type Name = string

type Id = int
type Level = int

[<StructuralEquality; StructuralComparison>]
type Type = 
    | TConst of Name              // type constant: int
    | TApp of Type * Type list    // type application: int list
    | TArrow of Type list * Type  // function type: `int -> int`
    | TVar of TypeVar ref         // type variable: `a`
    | TForall of Id list * Type   // polymorphic type: `forall[a] a -> a` 

and TypeVar =
    | Unbound of Id * Level
    | Link of Type
    | Generic of Id
    | Bound of Id

let rec unlink = function
    | TVar ({contents = Link ty} as tvar) ->
        let ty = unlink ty
        tvar := Link ty
        ty
    | ty -> ty

let rec is_monomorphic = function
    | TForall _ -> false
    | TConst _ -> true
    | TVar {contents = Link ty} -> is_monomorphic ty
    | TVar _ -> true
    | TApp (ty, ty_arg_list) ->
        is_monomorphic ty && ty_arg_list |> List.forall is_monomorphic 
    | TArrow(param_ty_list, return_ty) ->
        param_ty_list |> List.forall is_monomorphic && is_monomorphic return_ty


type TypeAnn = Id list * Type

type Expr =
    | Var of Name                                // variable
    | Call of Expr * Expr list                   // application
    | Fun of (Name * TypeAnn option) list * Expr // abstraction
    | Let of Name * Expr * Expr                  // let
    | Ann of Expr * TypeAnn                      // type annotation: `1 : int`

let rec is_annotated = function
    | Ann _ -> true
    | Let(_, _, body) -> is_annotated body
    | _ -> false

let extend_name_map (name_map: Map<Id, Name>) (var_id_list: Id list) : Name list * Map<Id, Name> =
    let name_list_rev, (name_map: Map<Id, Name>) =
        var_id_list
        |> List.fold 
            (fun (name_list, name_map) var_id ->
                let new_name = name_map.Count.ToString()
                new_name :: name_list, name_map.Add(var_id, new_name))
            ([], name_map)
    (List.rev name_list_rev, name_map)

let string_of_ty_with_bound_tvars name_map ty =
    let rec complex name_map = function
        | TArrow(param_ty_list, return_ty) ->
            TODO "Change this so it's not (arg1, arg2) -> return but arg1 -> arg2 -> return"
            let param_ty_list_str = 
                match param_ty_list with
                | [param_ty] -> simple name_map param_ty
                | _ -> "(" + String.concat ", " (param_ty_list |> List.map (complex name_map)) + ")"
            let return_ty_str = complex name_map return_ty
            param_ty_list_str + " -> " + return_ty_str
        | TForall(var_id_list, ty) ->
            let name_list, name_map = extend_name_map name_map var_id_list
            let name_list_str = String.concat " " name_list 
            "forall[" + name_list_str + "] " + complex name_map ty
        | TVar {contents = Link ty} -> complex name_map ty
        | ty -> simple name_map ty
    and simple name_map = function
        | TConst name -> name
        | TApp(ty, ty_arg_list) ->
            let ty_str = simple name_map ty
            let ty_arg_list_str = String.concat ", " (ty_arg_list |> List.map (complex name_map))
            ty_str + "[" + ty_arg_list_str + "]"
        | TVar {contents = Unbound(id, _)} -> "@unknown" + id.ToString()
        | TVar {contents = Bound id} -> name_map.Item id
        | TVar {contents = Generic id} -> "@generic" + id.ToString()
        | TVar {contents = Link ty} -> simple name_map ty
        | ty -> "(" + complex name_map ty + ")"
    complex name_map ty

let string_of_ty ty =
    string_of_ty_with_bound_tvars Map.empty ty

let string_of_ty_ann (var_id_list, ty) =
    let name_list, name_map = extend_name_map Map.empty var_id_list
    let ty_str = string_of_ty_with_bound_tvars name_map ty
    match name_list with
    | [] -> ty_str
    | _ -> "some[" + String.concat " " name_list + "] " + ty_str

let string_of_expr expr =
    let rec complex = function
        | Fun(param_list, body_expr) ->
            TODO "Change this so it's not (arg1, arg2) -> return but arg1 -> arg2 -> return"
            let param_list_str =
                String.concat " "
                    (List.map
                        (fun (param_name, maybe_ty_ann) ->
                            match maybe_ty_ann with
                            | Some ty_ann -> "(" + param_name + " : " + string_of_ty_ann ty_ann + ")"
                            | None -> param_name)
                        param_list)
            "fun " + param_list_str + " -> " + complex body_expr
        | Let(var_name, value_expr, body_expr) ->
            "let " + var_name + " = " + complex value_expr + " in " + complex body_expr
        | Ann(expr, ty_ann) ->
            simple expr + " : " + string_of_ty_ann ty_ann
        | expr -> simple expr
    and simple = function
        | Var name -> name
        | Call(fn_expr, arg_list) ->
            simple fn_expr + "(" + String.concat ", " (arg_list |> List.map complex) + ")"
        | expr -> "(" + complex expr + ")"
    complex expr