﻿module Stripe.Test.Propagate

open Microsoft.VisualStudio.TestTools.UnitTesting

open Microsoft.FSharp.Text.Lexing

open Stripe.Lexer
open Stripe.Parser
open Stripe.Expr
open Stripe.Infer
open Stripe.Util

open Prelude

[<CustomEquality; NoComparison>]
type Result =
    | OK of string
    | Fail of string option
with
    override x.Equals (yobj) =
        match yobj with
        | :? Result as y ->
            match x, y with
            | Fail None, Fail _ | Fail _, Fail None -> true
            | Fail (Some msg1), Fail (Some msg2) -> msg1 = msg2
            | OK ty_str1, OK ty_str2 -> ty_str1 = ty_str2
            | _ -> false
        | _ -> false
    override x.GetHashCode () = failwith "Not implemented"

let fail = Fail None
let error msg = Fail (Some msg)

[<TestClass>]
type ``Propagate tests`` () =
    let parseCode code =
        Stripe.Infer.reset_id ()
        try
            let lexbuf = LexBuffer<char>.FromString code
            let result = Stripe.Parser.expr_eof Stripe.Lexer.token lexbuf
            let ty = Stripe.Propagate.infer Prelude.core 0 None Stripe.Propagate.Generalized result
            OK <| string_of_ty ty
        with
        | Stripe.Infer.Error msg -> Fail (Some msg)

    let parseResult = function
        | OK ty_str ->
            let lexbuf = LexBuffer<char>.FromString ty_str
            let result = Stripe.Parser.ty_eof Stripe.Lexer.token lexbuf
            OK <| string_of_ty result
        | x -> x

    let (|=|) code result = 
        let parsedCode = parseCode code
        let parsedResult = parseResult result
        let result = parsedCode = parsedResult
        let parsedCodeString = sprintf "%A" parsedCode
        let parsedResultString = sprintf "%A" parsedResult
        Assert.IsTrue(result)

    [<TestMethod>]
    member x.``Test_1`` () =
        TODO "Rename everything"
        let input = "id"
        let result = OK "forall[a] a -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_2`` () =
        let input = "one"
        let result = OK "int"
        input |=| result

    [<TestMethod>]
    member x.``Test_3`` () =
        let input = "x"
        let result = error "variable x not found"
        input |=| result

    [<TestMethod>]
    member x.``Test_4`` () =
        let input = "let x = x in x"
        let result = error "variable x not found"
        input |=| result

    [<TestMethod>]
    member x.``Test_5`` () =
        let input = "let x = id in x"
        let result = OK "forall[a] a -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_6`` () =
        let input = "let x = fun y -> y in x"
        let result = OK "forall[a] a -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_7`` () =
        let input = "fun x -> x"
        let result = OK "forall[a] a -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_8`` () =
        let input = "pair"
        let result = OK "forall[a b] (a, b) -> pair[a, b]"
        input |=| result

    [<TestMethod>]
    member x.``Test_9`` () =
        let input = "fun x -> let y = fun z -> z in y"
        let result = OK "forall[a b] a -> b -> b"
        input |=| result

    [<TestMethod>]
    member x.``Test_10`` () =
        let input = "let f = fun x -> x in let id = fun y -> y in eq(f, id)"
        let result = OK "bool"
        input |=| result

    [<TestMethod>]
    member x.``Test_11`` () =
        let input = "let f = fun x -> x in let id = fun y -> y in eq_curry(f)(id)"
        let result = OK "bool"
        input |=| result

    [<TestMethod>]
    member x.``Test_12`` () =
        let input = "let f = fun x -> x in eq(f, succ)"
        let result = OK "bool"
        input |=| result

    [<TestMethod>]
    member x.``Test_13`` () =
        let input = "let f = fun x -> x in eq_curry(f)(succ)"
        let result = OK "bool"
        input |=| result

    [<TestMethod>]
    member x.``Test_14`` () =
        let input = "let f = fun x -> x in pair(f(one), f(true))"
        let result = OK "pair[int, bool]"
        input |=| result

    [<TestMethod>]
    member x.``Test_15`` () =
        let input = "fun f -> pair(f(one), f(true))"
        let result = fail
        input |=| result

    [<TestMethod>]
    member x.``Test_16`` () =
        let input = "let f = fun x y -> let a = eq(x, y) in eq(x, y) in f"
        let result = OK "forall[a] (a, a) -> bool"
        input |=| result

    [<TestMethod>]
    member x.``Test_17`` () =
        let input = "let f = fun x y -> let a = eq_curry(x)(y) in eq_curry(x)(y) in f"
        let result = OK "forall[a] (a, a) -> bool"
        input |=| result

    [<TestMethod>]
    member x.``Test_18`` () =
        let input = "id(id)"
        let result = OK "forall[a] a -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_19`` () =
        let input = "choose(fun x y -> x, fun x y -> y)"
        let result = OK "forall[a] (a, a) -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_20`` () =
        let input = "choose_curry(fun x y -> x)(fun x y -> y)"
        let result = OK "forall[a] (a, a) -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_21`` () =
        let input = "let x = id in let y = let z = x(id) in z in y"
        let result = OK "forall[a] a -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_22`` () =
        let input = "single(id)"
        let result = OK "forall[a] list[a -> a]"
        input |=| result

    [<TestMethod>]
    member x.``Test_23`` () =
        let input = "cons_curry(id)(nil)"
        let result = OK "forall[a] list[a -> a]"
        input |=| result

    [<TestMethod>]
    member x.``Test_24`` () =
        let input = "let lst1 = cons(id, nil) in let lst2 = cons(succ, lst1) in lst2"
        let result = OK "list[int -> int]"
        input |=| result

    [<TestMethod>]
    member x.``Test_25`` () =
        let input = "cons_curry(id)(cons_curry(succ)(cons_curry(id)(nil)))"
        let result = OK "list[int -> int]"
        input |=| result

    [<TestMethod>]
    member x.``Test_26`` () =
        let input = "plus(one, true)"
        let result = error "cannot unify types int and bool"
        input |=| result

    [<TestMethod>]
    member x.``Test_27`` () =
        let input = "plus(one)"
        let result = error "unexpected number of arguments"
        input |=| result

    [<TestMethod>]
    member x.``Test_28`` () =
        let input = "fun x -> let y = x in y"
        let result = OK "forall[a] a -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_29`` () =
        let input = "fun x -> let y = let z = x(fun x -> x) in z in y"
        let result = OK "forall[a b] ((a -> a) -> b) -> b"
        input |=| result

    [<TestMethod>]
    member x.``Test_30`` () =
        let input = "fun x -> fun y -> let x = x(y) in x(y)"
        let result = OK "forall[a b] (a -> a -> b) -> a -> b"
        input |=| result

    [<TestMethod>]
    member x.``Test_31`` () =
        let input = "fun x -> let y = fun z -> x(z) in y"
        let result = OK "forall[a b] (a -> b) -> a -> b"
        input |=| result

    [<TestMethod>]
    member x.``Test_32`` () =
        let input = "fun x -> let y = fun z -> x in y"
        let result = OK "forall[a b] a -> b -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_33`` () =
        let input = "fun x -> fun y -> let x = x(y) in fun x -> y(x)"
        let result = OK "forall[a b c] ((a -> b) -> c) -> (a -> b) -> a -> b"
        input |=| result

    [<TestMethod>]
    member x.``Test_34`` () =
        let input = "fun x -> let y = x in y(y)"
        let result = error "recursive types"
        input |=| result

    [<TestMethod>]
    member x.``Test_35`` () =
        let input = "fun x -> let y = fun z -> z in y(y)"
        let result = OK "forall[a b] a -> b -> b"
        input |=| result

    [<TestMethod>]
    member x.``Test_36`` () =
        let input = "fun x -> x(x)"
        let result = error "recursive types"
        input |=| result

    [<TestMethod>]
    member x.``Test_37`` () =
        let input = "one(id)"
        let result = error "expected a function"
        input |=| result

    [<TestMethod>]
    member x.``Test_38`` () =
        let input = "fun f -> let x = fun g y -> let _ = g(y) in eq(f, g) in x"
        let result = OK "forall[a b] (a -> b) -> (a -> b, a) -> bool"
        input |=| result

    [<TestMethod>]
    member x.``Test_39`` () =
        let input = "let const = fun x -> fun y -> x in const"
        let result = OK "forall[a b] a -> b -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_40`` () =
        let input = "let apply = fun f x -> f(x) in apply"
        let result = OK "forall[a b] (a -> b, a) -> b"
        input |=| result

    [<TestMethod>]
    member x.``Test_41`` () =
        let input = "let apply_curry = fun f -> fun x -> f(x) in apply_curry"
        let result = OK "forall[a b] (a -> b) -> a -> b"
        input |=| result

  (* HMF *)
    [<TestMethod>]
    member x.``Test_42`` () =
        let input = "ids"
        let result = OK "list[forall[a] a -> a]"
        input |=| result

    [<TestMethod>]
    member x.``Test_43`` () =
        let input = "fun f -> pair(f(one), f(true))"
        let result = fail
        input |=| result

    [<TestMethod>]
    member x.``Test_44`` () =
        let input = "fun (f : forall[a] a -> a) -> pair(f(one), f(true))"
        let result = OK "(forall[a] a -> a) -> pair[int, bool]"
        input |=| result

    [<TestMethod>]
    member x.``Test_45`` () =
        let input = "cons(ids, nil)"
        let result = OK "list[list[forall[a] a -> a]]"
        input |=| result

    [<TestMethod>]
    member x.``Test_46`` () =
        let input = "choose(nil, ids)"
        let result = OK "list[forall[a] a -> a]"
        input |=| result

    [<TestMethod>]
    member x.``Test_47`` () =
        let input = "choose(ids, nil)"
        let result = OK "list[forall[a] a -> a]"
        input |=| result

    [<TestMethod>]
    member x.``Test_48`` () =
        let input = "cons(fun x -> x, ids)"
        let result = OK "list[forall[a] a -> a]"
        input |=| result

    [<TestMethod>]
    member x.``Test_49`` () =
        let input = "let rev_cons = fun x y -> cons(y, x) in rev_cons(ids, id)"
        let result = OK "list[forall[a] a -> a]"
        input |=| result

    [<TestMethod>]
    member x.``Test_50`` () =
        let input = "cons(id, ids)"
        let result = OK "list[forall[a] a -> a]"
        input |=| result

    [<TestMethod>]
    member x.``Test_51`` () =
        let input = "cons(id, cons(succ, nil))"
        let result = OK "list[int -> int]"
        input |=| result

    [<TestMethod>]
    member x.``Test_52`` () =
        let input = "poly(id)"
        let result = OK "pair[int, bool]"
        input |=| result

    [<TestMethod>]
    member x.``Test_53`` () =
        let input = "poly(fun x -> x)"
        let result = OK "pair[int, bool]"
        input |=| result

    [<TestMethod>]
    member x.``Test_54`` () =
        let input = "poly(succ)"
        let result = fail
        input |=| result

    [<TestMethod>]
    member x.``Test_55`` () =
        let input = "apply(succ, one)"
        let result = OK "int"
        input |=| result

    [<TestMethod>]
    member x.``Test_56`` () =
        let input = "rev_apply(one, succ)"
        let result = OK "int"
        input |=| result

    [<TestMethod>]
    member x.``Test_57`` () =
        let input = "apply(poly, id)"
        let result = OK "pair[int, bool]"
        input |=| result

    [<TestMethod>]
    member x.``Test_58`` () =
        let input = "apply_curry(poly)(id)"
        let result = OK "pair[int, bool]"
        input |=| result

    [<TestMethod>]
    member x.``Test_59`` () =
        let input = "rev_apply(id, poly)"
        let result = OK "pair[int, bool]"
        input |=| result

    [<TestMethod>]
    member x.``Test_60`` () =
        let input = "rev_apply_curry(id)(poly)"
        let result = fail
        input |=| result

    [<TestMethod>]
    member x.``Test_61`` () =
        let input = "(id : forall[a] a -> a) : int -> int"
        let result = OK "int -> int"
        input |=| result

    [<TestMethod>]
    member x.``Test_62`` () =
        let input = "single(id : forall[a] a -> a)"
        let result = OK "list[forall[a] a -> a]"
        input |=| result

    [<TestMethod>]
    member x.``Test_63`` () =
        let input = "(fun x -> fun y -> let z = choose(x, y) in z)(id : forall[a] a -> a)"
        let result = OK "(forall[a] a -> a) -> (forall[a] a -> a)"
        input |=| result

    [<TestMethod>]
    member x.``Test_64`` () =
        let input = "fun (x : forall[a] a -> a) -> x"
        let result = OK "forall[a] (forall[b] b -> b) -> a -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_65`` () =
        let input = "id_id(id)"
        let result = OK "forall[a] a -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_66`` () =
        let input = "almost_id_id(id)"
        let result = OK "forall[a] a -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_67`` () =
        let input = "fun id -> poly(id)"
        let result = fail
        input |=| result

    [<TestMethod>]
    member x.``Test_68`` () =
        let input = "fun ids -> id_ids(ids)"
        let result = fail
        input |=| result

    [<TestMethod>]
    member x.``Test_69`` () =
        let input = "poly(id(id))"
        let result = OK "pair[int, bool]"
        input |=| result

    [<TestMethod>]
    member x.``Test_70`` () =
        let input = "length(ids)"
        let result = OK "int"
        input |=| result

    [<TestMethod>]
    member x.``Test_71`` () =
        let input = "map(head, single(ids))"
        let result = OK "list[forall[a] a -> a]"
        input |=| result

    [<TestMethod>]
    member x.``Test_72`` () =
        let input = "map_curry(head)(single(ids))"
        let result = OK "list[forall[a] a -> a]"
        input |=| result

    [<TestMethod>]
    member x.``Test_73`` () =
        let input = "apply(map_curry(head), single(ids))"
        let result = OK "list[forall[a] a -> a]"
        input |=| result

    [<TestMethod>]
    member x.``Test_74`` () =
        let input = "apply_curry(map_curry(head))(single(ids))"
        let result = OK "list[forall[a] a -> a]"
        input |=| result

    [<TestMethod>]
    member x.``Test_75`` () =
        let input = "apply(id, one)"
        let result = OK "int"
        input |=| result

    [<TestMethod>]
    member x.``Test_76`` () =
        let input = "apply_curry(id)(one)"
        let result = OK "int"
        input |=| result

    [<TestMethod>]
    member x.``Test_77`` () =
        let input = "poly(magic)"
        let result = OK "pair[int, bool]"
        input |=| result

    [<TestMethod>]
    member x.``Test_78`` () =
        let input = "id_magic(magic)"
        let result = OK "forall[a b] a -> b"
        input |=| result

    [<TestMethod>]
    member x.``Test_79`` () =
        let input = "fun (f : forall[a b] a -> b) -> let a = id_magic(f) in one"
        let result = OK "(forall[a b] a -> b) -> int"
        input |=| result

    [<TestMethod>]
    member x.``Test_80`` () =
        let input = "fun (f : some[a b] a -> b) -> id_magic(f)"
        let result = fail
        input |=| result

    [<TestMethod>]
    member x.``Test_81`` () =
        let input = "id_magic(id)"
        let result = fail
        input |=| result

    [<TestMethod>]
    member x.``Test_82`` () =
        let input = "fun (f : forall[a b] a -> b) -> f : forall[a] a -> a"
        let result = OK "(forall[a b] a -> b) -> (forall[a] a -> a)"
        input |=| result

    [<TestMethod>]
    member x.``Test_83`` () =
        let input = "let const = (any : forall[a] a -> (forall[b] b -> a)) in const(any)"
        let result = OK "forall[a b] a -> b"
        input |=| result


  (* propagation of types *)
    [<TestMethod>]
    member x.``Test_84`` () =
        let input = "single(id) : list[forall[a] a -> a]"
        let result = OK "list[forall[a] a -> a]"
        input |=| result

    [<TestMethod>]
    member x.``Test_85`` () =
        let input = "id(single(id)) : list[forall[a] a -> a]"
        let result = fail
        input |=| result

    [<TestMethod>]
    member x.``Test_86`` () =
        let input = "cons(single(id), single(ids))"
        let result = OK "list[list[forall[a] a -> a]]"
        input |=| result

    [<TestMethod>]
    member x.``Test_87`` () =
        let input = "id_id(id) : int -> int"
        let result = OK "int -> int"
        input |=| result

    [<TestMethod>]
    member x.``Test_88`` () =
        let input = "head(ids)(one) : int"
        let result = OK "int"
        input |=| result

    [<TestMethod>]
    member x.``Test_89`` () =
        let input = "head(ids) : int -> int"
        let result = OK "int -> int"
        input |=| result

    [<TestMethod>]
    member x.``Test_90`` () =
        let input = "let f = head(ids) in f : int -> int"
        let result = OK "int -> int"
        input |=| result

    [<TestMethod>]
    member x.``Test_91`` () =
        let input = "cons(single(id) : list[forall[a] a -> a], single(single(fun x -> x)))"
        let result = OK "list[list[forall[a] a -> a]]"
        input |=| result

    [<TestMethod>]
    member x.``Test_92`` () =
        let input = "id_succ(head(map(id, ids)))"
        let result = OK "int -> int"
        input |=| result

    [<TestMethod>]
    member x.``Test_93`` () =
        let input = "(fun f -> f(f)) : (forall[a] a -> a) -> (forall[a] a -> a)"
        let result = OK "(forall[a] a -> a) -> (forall[a] a -> a)"
        input |=| result

    [<TestMethod>]
    member x.``Test_94`` () =
        let input = "(fun f -> f(f)) : forall[b] (forall[a] a -> a) -> b -> b"
        let result = OK "forall[a] (forall[b] b -> b) -> a -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_95`` () =
        let input = "(let x = one in (fun f -> pair(f(x), f(true)))) : (forall[a] a -> a) -> pair[int, bool]"
        let result = OK "(forall[a] a -> a) -> pair[int, bool]"
        input |=| result

    [<TestMethod>]
    member x.``Test_96`` () =
        let input = 
            "let returnST = any : forall[a s] a -> ST[s, a] in " +
            "returnST(one) : forall[s] ST[s, int]"
        let result = OK "forall[s] ST[s, int]"
        input |=| result

    [<TestMethod>]
    member x.``Test_97`` () =
        let input = "special(fun f -> f(f))"
        let result = OK "forall[a] a -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_98`` () =
        let input = "apply(special, fun f -> f(f))"
        let result = OK "forall[a] a -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_99`` () =
        let input = "rev_apply(fun f -> f(f), special)"
        let result = OK "forall[a] a -> a"
        input |=| result

    [<TestMethod>]
    member x.``Test_100`` () =
        let input = "apply(fun f -> choose(id_id, f), id_id : (forall[a] a -> a) -> (forall[a] a -> a))"
        let result = OK "(forall[a] a -> a) -> (forall[a] a -> a)"
        input |=| result

    [<TestMethod>]
    member x.``Test_101`` () =
        let input = "rev_apply(id_id : (forall[a] a -> a) -> (forall[a] a -> a), fun f -> choose(id_id, f))"
        let result = OK "(forall[a] a -> a) -> (forall[a] a -> a)"
        input |=| result
