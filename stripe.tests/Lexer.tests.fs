﻿module Stripe.Test.Lexer

open Microsoft.VisualStudio.TestTools.UnitTesting

open Microsoft.FSharp.Text.Lexing

open Stripe.Lexer
open Stripe.Parser

[<TestClass>]
type ``Lexer tests`` () =
    let parse text =
        let lexbuf = LexBuffer<char>.FromString text
        let rec f acc =
            match Stripe.Lexer.token lexbuf with
            | EOF -> acc
            | token -> f (token :: acc)
        List.rev (f [])

    let (|=|) str out = 
        Assert.IsTrue(parse str = out)

    [<TestMethod>]
    member x.``An empty file is valid`` () =
        let input = ""
        let result = []
        input |=| result

    [<TestMethod>]
    member x.``We handle tabs, new lines correctly`` () =
        let input = " \t\n\n\t\r\n\r"
        let result = []
        input |=| result

    [<TestMethod>]
    member x.``A bunch of accepted tokens`` () =
        let input = "())in,:][let_ _lMa->=="
        let result = [ 
            LPAREN; RPAREN; RPAREN; IN; COMMA; COLON; RBRACKET; 
            LBRACKET; IDENT "let_"; IDENT "_lMa"; ARROW; EQUALS; EQUALS
        ]
        input |=| result

    [<TestMethod>]
    member x.``Our different tokens`` () =
        let input = "let fun in forall some"
        let result = [ LET; FUN; IN; FORALL; SOME]
        input |=| result

    [<TestMethod>]
    [<ExpectedException(typeof<Stripe.Lexer.Error>)>]
    member x.``Only a semicolon, fails`` () =
        let input = ";"
        parse input |> ignore