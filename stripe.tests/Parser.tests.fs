﻿module Stripe.Test.Parser

open Microsoft.VisualStudio.TestTools.UnitTesting

open Microsoft.FSharp.Text.Lexing

open Stripe.Lexer
open Stripe.Parser
open Stripe.Expr
open Stripe.Infer
open Stripe.Util

[<TestClass>]
type ``Parser tests`` () =
    let parse code =
        Stripe.Infer.reset_id ()
        let lexbuf = LexBuffer<char>.FromString code
        let result = Stripe.Parser.expr_eof Stripe.Lexer.token lexbuf
        result
        (*
        let expr_str = string_of_expr result
        Stripe.Infer.reset_id ()
        let lexbuf = LexBuffer<char>.FromString expr_str
        let new_result = Stripe.Parser.expr_eof Stripe.Lexer.token lexbuf
        new_result
        *)

    let bound i = TVar (ref (Bound i))

    let (|=|) str out = 
        let parsed = parse str
        let result = parsed = out
        let parsedString = sprintf "%A" parsed
        let outString = sprintf "%A" out
        Assert.IsTrue(result)

    [<TestMethod>]
    [<ExpectedException(typeof<System.Exception>)>]
    member x.``An empty file is invalid`` () =
        let input = ""
        parse input |> ignore

    [<TestMethod>]
    member x.``Single variable`` () =
        let input = "a"
        let result = (Var "a")
        input |=| result

    [<TestMethod>]
    member x.``Call function with two arguments`` () =
        let input = "f(x, y)"
        let result = (Call (Var "f", [Var "x"; Var "y"]))
        input |=| result

    [<TestMethod>]
    member x.``Call a function which calls a function`` () =
        let input = "f(x)(y)"
        let result = (Call(Call(Var "f", [Var "x"]), [Var "y"]))
        input |=| result

    [<TestMethod>]
    member x.``A lambda enclosed in a function calling itself`` () =
        let input = "let f = fun x y -> g(x, y) in f(a, b)"
        let result = (Let("f", Fun([("x", None); ("y", None)], Call(Var "g", [Var "x"; Var "y"])), Call(Var "f", [Var "a"; Var "b"])))
        input |=| result

    [<TestMethod>]
    member x.``Assign then call`` () =
        let input = 
            "let x = a in " +
            "let y = b in " +
            "f(x, y)"
        let result = (Let("x", Var "a", Let("y", Var "b", Call(Var "f", [Var "x"; Var "y"]))))
        input |=| result
    
    [<TestMethod>]
    [<ExpectedException(typeof<System.Exception>)>]
    member x.``Can't call a function without parenthesis`` () =
        TODO "This should not stay true"
        let input = "f x"
        parse input

    [<TestMethod>]
    [<ExpectedException(typeof<System.Exception>)>]
    member x.``A let without a in fails`` () =
        TODO "This should not stay true"
        let input = "let a = one"
        parse input

    [<TestMethod>]
    [<ExpectedException(typeof<System.Exception>)>]
    member x.``Tuple will not work`` () =
        TODO "This should not stay true"
        let input = "a, b"
        parse input

    [<TestMethod>]
    [<ExpectedException(typeof<System.Exception>)>]
    member x.``Simple equal does not work`` () =
        let input = "a = b"
        parse input

    [<TestMethod>]
    [<ExpectedException(typeof<System.Exception>)>]
    member x.``Unit does not work`` () =
        TODO "This should not stay true"
        let input = "()"
        parse input

    [<TestMethod>]
    [<ExpectedException(typeof<System.Exception>)>]
    member x.``Lambdas taking a tuple does not work`` () =
        TODO "This should not stay true"
        let input = "fun x, y -> y"
        parse input

    [<TestMethod>]
    member x.``I'm not sure why this is valid`` () =
        TODO "I'm not sure why this is valid"
        let input = "fun -> y"
        let result = (Fun([], Var "y"))
        input |=| result

    [<TestMethod>]
    member x.``Type annotation`` () =
        let input = "id : forall[a] a -> a"
        let result = Ann(Var "id", ([], TForall([0], TArrow([bound 0], bound 0))))
        input |=| result

    [<TestMethod>]
    member x.``Multiple annotations`` () =
        let input = "magic : forall[a b] a -> b"
        let result = Ann( Var "magic", ([], TForall([0; 1], TArrow([bound 0], bound 1))))
        input |=| result

    [<TestMethod>]
    member x.``Bounded annotations`` () =
        let input = "magic : forall[x int] x -> int"
        let result = Ann( Var "magic", ([], TForall([0; 1], TArrow([bound 0], bound 1))))
        input |=| result

    [<TestMethod>]
    member x.``Useless annotations`` () =
        let input = "magic : forall[w x y z] y -> x"
        let result = Ann( Var "magic", ([], TForall([0; 1], TArrow([bound 0], bound 1))))
        input |=| result

    [<TestMethod>]
    [<ExpectedException(typeof<System.Exception>)>]
    member x.``Two forall type annotations is not valid`` () =
        let input = "a : forall[a] forall[b] b"
        parse input

    [<TestMethod>]
    member x.``Binding a simple type`` () =
        let input = "a : (forall[int] int -> int) -> int"
        let result = Ann(Var "a", ([], TArrow([TForall([0], TArrow([bound 0], bound 0))], TConst "int")))
        input |=| result

    [<TestMethod>]
    member x.``Simple function annotation`` () =
        let input = "f : int -> int -> int"
        let result = Ann(Var "f", ([], TArrow([TConst "int"], TArrow([TConst "int"], TConst "int"))))
        input |=| result

    [<TestMethod>]
    member x.``Useless some and forall type annotation`` () =
        let input = "f : some[a b] forall[c d] (int, int) -> int"
        let result = Ann(Var "f", ([], TArrow([TConst "int"; TConst "int"], TConst "int")))
        input |=| result

    [<TestMethod>]
    member x.``Complex annotation`` () =
        let input = "fun (x : some[a] a -> a) y (z : (list[forall[t] t -> int])) -> m : int"
        let result =
            Fun([("x", Some ([0], TArrow([bound 0], bound 0))); ("y", None);
                ("z", Some ([], TApp(TConst "list", [TForall([1], TArrow([bound 1], TConst "int"))])))],
                Ann(Var "m", ([], TConst "int")))
        input |=| result